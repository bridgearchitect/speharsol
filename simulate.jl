# include modules
using FastSphericalHarmonics
include("alltypes.jl")
include("write.jl")
include("integrate.jl")

# function to calculate spherical harmonic transformation
function calculateSphTransormation(c)

    # calculate harmonic transformation
    c_harm = sph_transform(c)
    # return calculated array
    return c_harm

end

# function to calculate nonlinear function using original function
function calculateNonlinearFunction(c_orig, thetaArr, phiArr, rho_s, ca, cb)

    # create array for nonlinear function
    f_orig = zeros(size(c_orig))

    # go through all coefficients
    i = 1
    for thetaI in thetaArr
        j = 1
        for phiI in phiArr
            f_orig[i,j] = 2.0 * rho_s * (ca - c_orig[i,j]) * (-cb + c_orig[i,j]) * (ca + cb - 2.0 * c_orig[i,j])
            j = j + 1
        end
        i = i + 1
    end

    # return calculated array
    return f_orig

end

# function to calculate time evolution using transformed function in spherical harmonics
function calculateTimeEvolution(c_harm, f_harm, tstep, kappa, M, R, thetaArr, phiArr)

    # create array for new values of spherical harmonic representation
    cnew_harm = zeros(size(c_harm))
    # calculate Laplace operator for spherical harmonic representation considering different variants
    lapl_c_harm = sph_laplace(c_harm)
    lapl2_c_harm = sph_laplace(lapl_c_harm)
    lapl_f_harm = sph_laplace(f_harm)

    # go through all coefficients
    i = 1
    for thetaI in thetaArr
        j = 1
        for phiI in phiArr
            # normalize spherical harmonics for the specified sphere
            lapl_c_harm[i,j] = lapl_c_harm[i,j] / (R * R)
            lapl2_c_harm[i,j] = lapl2_c_harm[i,j] / (R * R * R * R)
            lapl_f_harm[i,j] = lapl_f_harm[i,j] / (R * R)
            j = j + 1
        end
        i = i + 1
    end

    # go through all coefficients
    i = 1
    for thetaI in thetaArr
        j = 1
        for phiI in phiArr
            # calculate new value for original
            cnew_harm[i,j] = c_harm[i,j] + M * tstep * lapl_f_harm[i,j] - M * kappa *
                tstep * lapl2_c_harm[i,j]
            j = j + 1
        end
        i = i + 1
    end

    # return calculated new array
    return cnew_harm

end

# function to launch simulation for phase field equation using Euler forward scheme
function launchSimulationForward(c, thetaArr, phiArr, tmin, tmax, tstep, ca, cb, rho_s, rho_e, kappa, M, R)

    # initialization of variables
    tcur = tmin
    i = 0

    # calculate spherical harmonic transformation for initial function
    c_harm = calculateSphTransormation(c)
    # calculate inversed spherical harmonic transformation
    c_orig = sph_evaluate(c_harm)

    # calculate initial mass and energy of system
    mass = calculateMassSystem(c_orig, thetaArr, phiArr, R)
    energy = calculateEnergySystem(c_orig, thetaArr, phiArr, R, kappa, rho_e, ca, cb)

    # write the first snapshot in system
    writeSnapshotSystem(c_orig, thetaArr, phiArr, tcur)
    # write header row in parametric file
    writeHeaderParametricFile()
    # write initial global values in parametric file
    writeGlobalValuesParametricFile(tcur, mass, energy)

    # go to the next step
    tcur = tcur + tstep
    i = i + 1
    # go through time domain
    while tcur <= tmax
        # calculate non-linear function f using original function
        f_orig = calculateNonlinearFunction(c_orig, thetaArr, phiArr, rho_s, ca, cb)
        # calculate spherical harmonic image for non-linear function f
        f_harm = calculateSphTransormation(f_orig)
        # calculate time evolution of physical system
        c_harm = calculateTimeEvolution(c_harm, f_harm, tstep, kappa, M, R, thetaArr, phiArr)
        # calculate inversed spherical harmonic transformation
        c_orig = sph_evaluate(c_harm)
        # calculate energy and mass of system
        mass = calculateMassSystem(c_orig, thetaArr, phiArr, R)
        energy = calculateEnergySystem(c_orig, thetaArr, phiArr, R, kappa, rho_e, ca, cb)
        # write snapshot (state of system) it it is essential
        if (i % snapshotPeriod == 0) || (abs(tcur - tmax) < smallNumber)
            writeSnapshotSystem(c_orig, thetaArr, phiArr, tcur)
        end
        # write global values in parametric file if it is essential
        if (i % paramPeriod == 0) || (abs(tcur - tmax) < smallNumber)
            # write energy and mass of system in parametric file
            writeGlobalValuesParametricFile(tcur, mass, energy)
        end
        # write simulation info in terminal if it is essential
        if (i % terminalPeriod == 0) || (abs(tcur - tmax) < smallNumber)
            # write energy and mass of system in terminal
            writeSimulationInfoInTerminal(tcur, mass, energy)
        end
        # go to the next step
        tcur = tcur + tstep
        i = i + 1
    end

end

# function to launch simulation for phase field equation using Euler semi-implicit scheme
function launchSimulationSemi(c, thetaArr, phiArr, tmin, tmax, tstep, ca, cb, rho_s, rho_e, kappa, M, R)

    # initialization of variables
    tcur = tmin
    i = 0

    # calculate spherical harmonic transformation for initial function
    c_harm = calculateSphTransormation(c)
    # calculate inversed spherical harmonic transformation
    c_orig = sph_evaluate(c_harm)

    # calculate initial mass and energy of system
    mass = calculateMassSystem(c_orig, thetaArr, phiArr, R)
    energy = calculateEnergySystem(c_orig, thetaArr, phiArr, R, kappa, rho_e, ca, cb)

    # write the first snapshot in system
    writeSnapshotSystem(c_orig, thetaArr, phiArr, tcur)
    # write header row in parametric file
    writeHeaderParametricFile()
    # write initial global values in parametric file
    writeGlobalValuesParametricFile(tcur, mass, energy)

    # go to the next step
    tcur = tcur + tstep
    i = i + 1
    # go through time domain
    while tcur <= tmax
        # calculate non-linear function f using original function
        f_orig = calculateNonlinearFunction(c_orig, thetaArr, phiArr, rho_s, ca, cb)
        # calculate spherical harmonic image for non-linear function f
        f_harm = calculateSphTransormation(f_orig)
        # calculate intermediate value of function c using Euler forward scheme
        c_inter_harm = calculateTimeEvolution(c_harm, f_harm, tstep, kappa, M, R, thetaArr, phiArr)
        # calculate inversed spherical harmonic transformation for intermediate value of function
        c_inter_orig = sph_evaluate(c_inter_harm)
        # calculate intermediate value of non-linear function f using original function
        f_inter_orig = calculateNonlinearFunction(c_inter_orig, thetaArr, phiArr, rho_s, ca, cb)
        # calculate intermediate spherical harmonic image for non-linear function
        f_inter_harm = calculateSphTransormation(f_inter_orig)
        # calculate final value of function c using Euler forward scheme and intermediate value of function f
        c_fin_harm = calculateTimeEvolution(c_harm, f_inter_harm, tstep, kappa, M, R, thetaArr, phiArr)
        c_harm = c_fin_harm
        # calculate inversed spherical harmonic transformation
        c_orig = sph_evaluate(c_fin_harm)
        # write snapshot (state of system) it it is essential
        if (i % snapshotPeriod == 0) || (abs(tcur - tmax) < smallNumber)
            writeSnapshotSystem(c_orig, thetaArr, phiArr, tcur)
        end
        # write initial global values in parametric file it it is essential
        if (i % paramPeriod == 0) || (abs(tcur - tmax) < smallNumber)
            # calculate energy and mass of system
            mass = calculateMassSystem(c_orig, thetaArr, phiArr, R)
            energy = calculateEnergySystem(c_orig, thetaArr, phiArr, R, kappa, rho_e, ca, cb)
            # write energy and mast of system in parametric file
            writeGlobalValuesParametricFile(tcur, mass, energy)
        end
        # go to the next step
        tcur = tcur + tstep
        i = i + 1
    end

end
