include("alltypes.jl")

# function to prepare array of coordinates
function prepareCoordinates(lmax)

    # create array for coordainates
    thetaArr, phiArr = sph_points(lmax + 1)
    # return created arrays
    return thetaArr, phiArr

end

# function to get array of initial function using code
function getInitialFunctionCode(thetaArr, phiArr, c0, eps)

    # calculate values of function
    c = [c0 + eps * (cos(8.0 * thetaI) * cos(15.0 * phiI) +
        (cos(12.0 * thetaI) * cos(10.0 * phiI))^2 + cos(2.5 * thetaI - 1.5 * phiI) *
        cos(7.0 * thetaI - 2.0 * phiI)) for thetaI in thetaArr,
        phiI in phiArr]
    # return created array
    return c

end

# function to get array of initial function using file
function getInitialFunctionFile(thetaArr, phiArr, filename)

    # initialize array for simulation
    c = [0.0 for thetaI in thetaArr, phiI in phiArr]
    # open file
    file = open(filename, "r")
    # read header line
    line = readline(file)

    # go through all angle values
    i = 1
    for thetaI in thetaArr
        # read new line for analyzing
        line = readline(file)
        # divide line into substrings
        substrings = split(line, " ")
        j = 1
        for phiI in phiArr
            # convert string to number
            cnum = parse(Float64, substrings[4 * j])
            # save converted number
            c[i,j] = cnum
            j = j + 1
        end
        i = i + 1
    end

    # close file
    close(file)
    # return created array
    return c

end

# function to prepare initial function for simulation
function prepareInitialFunction(thetaArr, phiArr, typeSource)

    # consider different variants
    if typeSource == codeSource
        # return initial function from code
        return getInitialFunctionCode(thetaArr, phiArr, c0, eps)
    elseif typeSource == fileSource
        # return initial function from file
        return getInitialFunctionFile(thetaArr, phiArr, filenameInitState)
    end

end
