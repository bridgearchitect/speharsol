# SpeHarSol

## Getting started

**SpeHarSol** (spectral harmonics solver) is the special software to solve partial
differential equations on curved surface (in this case, Cahn-Hilliard equation on sphere)
using spherical harmonics methods. The project is developed in a programming language **Julia**.

## Usage

To start simulation, you must enter the following command inside the project:

```bash
julia speharsol.jl
```
The simulation parameters are in the file **alltypes.jl**. They can be modified to suit
your needs. During the simulation, two types of files will be generated, namely parametric
and temporal. The parametric file stores the time dynamics of mass and energy in tabular
format, and the temporal file stores the state of the system at a certain point in time.

You can use the project module **plot.jl** to convert the state of system from text to
graphic format. Here you need to run the following command:

```bash
julia plot.jl <stateFile> <index>
```

Here, **\<stateFile\>** is name of text file to store state of system and and **\<index\>**
is index number for generated file of graphic format.

## Dependency

Running the program requires that two libraries must be installed, namely
the **FastSphericalHarmonics.jl** and **Plots.jl** libraries. It is easy to do using
Pkj.jl module inside Julia programming environment.

## Results

Results of simulations for NIST test are located in the folder **results**. There are plots, figures of
sphere states, Wolfram Mathematica scripts for further processing and tabular files of time dynamics.

## License

This software has MIT License. The text of this license is located below.

Copyright (c) 2023 Artem Tomilo

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

