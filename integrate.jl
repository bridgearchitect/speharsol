# include modules
include("alltypes.jl")

# define generic function for integration using trapezoidal approach
function genMathFunction(theta, phi, value)

    # calculate value of generic function
    g = (sin(theta))^(2 * q - 1) * (q * (cos(theta))^2 + (sin(theta))^2) * value /
        ((sin(theta))^(2 * q) + (cos(theta))^2)^1.5
    # return calculated value
    return g

end

# function to calculate mass of physical system using original function
function calculateMassSystem(c_orig, thetaArr, phiArr, R)

    # initialization
    mass = 0.0
    dTheta = thetaArr[2] - thetaArr[1]
    dPhi = phiArr[2] - phiArr[1]

    # go through all points
    i = 1
    for thetaI in thetaArr
        j = 1
        for phiJ in phiArr
            # calculate new portion of mass
            mass += dTheta * dPhi * R^2 * genMathFunction(thetaI, phiJ, c_orig[i,j])
            j = j + 1
        end
        i = i + 1
    end

    # return value of mass
    return mass

end

# function to calculate energy of physical system using original function
function calculateEnergySystem(c_orig, thetaArr, phiArr, R, kappa, rho_e, ca, cb)

    # initialization
    energy = 0.0
    dTheta = thetaArr[2] - thetaArr[1]
    dPhi = phiArr[2] - phiArr[1]
    # define number of points along two angles
    nTheta = size(thetaArr, 1)
    nPhi = size(phiArr, 1)

    # go through all points
    for i in 1:nTheta
        for j in 1:nPhi
            # initialization
            theta = thetaArr[i]
            phi = phiArr[j]
            c = c_orig[i, j]
            # calculate chemical energy
            fchem = rho_e * (c - ca)^2 * (c - cb)^2
            # calculate gradient along theta angle
            gradTheta = 0.0
            if i != nTheta
                cp = c_orig[i + 1, j]
                gradTheta = (1.0 / R) * (cp - c) / dTheta
            else
                cm = c_orig[i - 1, j]
                gradTheta = (1.0 / R) * (c - cm) / dTheta
            end
            # calculate gradient along phi angle
            gradPhi = 0.0
            if j != nPhi
                cp = c_orig[i, j + 1]
                gradPhi = (1.0 / (R * sin(theta))) * (cp - c) / dPhi
            else
                cm = c_orig[i, j - 1]
                gradPhi = (1.0 / (R * sin(theta))) * (c - cm) / dPhi
            end
            # calculate square of the length for gradient
            sqGrad = gradTheta * gradTheta + gradPhi * gradPhi
            # calculate new portion of energy
            energy += dTheta * dPhi * R^2 * genMathFunction(theta, phi, fchem + kappa * sqGrad / 2.0)
        end
    end

    # return value of mass
    return energy

end
