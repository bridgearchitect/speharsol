# define solver constants
lmax = 50
tmin = 40000.0
tmax = 1000000.0
c0 = 0.5
eps = 0.05
tstep = 0.2
snapshotPeriod = 5000
paramPeriod = 250
terminalPeriod = 500
smallNumber = 0.00000000001

# define additional solver constants
cmin = 0.348
cmax = 0.652
l = 0.05

# define physical constants
ca = 0.3
cb = 0.7
rho_e = 5.0
rho_s = 2.0
M = 5.0
kappa = 2.0
R = 100.0

# define math constant
q = 2.5

# define string constants
typeSource = "file"
typeSolver = "es"
typeLaunch = "test"

# define filename constants
filenameSnapshot = "result"
filenameParam = "parameters"
filenameInitState = "initState.dat"

# define macro expressions (type of sources)
fileSource = "file"
codeSource = "code"

# define macro expressions (type of solvers)
efSolver = "ef"
esSolver = "es"

# define macro expressions (type of launches)
testLaunch = "test"
simlLaunch = "siml"
