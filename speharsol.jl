# include modules
include("alltypes.jl")
include("prepare.jl")
include("simulate.jl")
include("write.jl")

# prepare coordinates for physical system
thetaArr, phiArr = prepareCoordinates(lmax)
# prepare array of initial conditions
c = prepareInitialFunction(thetaArr, phiArr, typeSource)

# print basic information in terminal
writeBasicInfoInTerminal(thetaArr)
# consider all possible situations for new launch
if typeLaunch == testLaunch
    # print message
    println("Test launch. Only important parameters were printed!")
    # close program
    exit()
elseif typeLaunch == simlLaunch
    # print message
    println("Simulation will be launched now!")
end

# launch simulation considering different types of solvers
if typeSolver == efSolver
    launchSimulationForward(c, thetaArr, phiArr, tmin, tmax, tstep, ca, cb, rho_s, rho_e, kappa, M, R)
elseif typeSolver == esSolver
    launchSimulationSemi(c, thetaArr, phiArr, tmin, tmax, tstep, ca, cb, rho_s, rho_e, kappa, M, R)
end
