# include plot library
using Plots
plotlyjs()

# define string constants
filenameImage = "sphere"

# function receive parameters from terminal
function receiveParametersFromFilename()
    # give parameters from terminal
    return ARGS[1], parse(Int, ARGS[2])
end

# function to read data (coordinates and value function) from file
function readDataFromFile(filename)

    # open file
    file = open(filename, "r")
    # read the first line
    line = readline(file)
    # receive information about number of angles
    substrings = split(line, " ")
    numTheta = parse(Int, substrings[1])
    numPhi = parse(Int, substrings[2])

    # create arrays to save data about coordinates and value function
    x = Array{Float64, 2}(undef, numTheta, numPhi)
    y = Array{Float64, 2}(undef, numTheta, numPhi)
    z = Array{Float64, 2}(undef, numTheta, numPhi)
    c = Array{Float64, 2}(undef, numTheta, numPhi)

    # go through all angles
    for i in 1:numTheta
        # read new line
        line = readline(file)
        # split line into substrings
        substrings = split(line, " ")
        for j in 1:numPhi
            # save received information in arrays
            x[i, j] = parse(Float64, substrings[4 * (j - 1) + 1])
            y[i, j] = parse(Float64, substrings[4 * (j - 1) + 2])
            z[i, j] = parse(Float64, substrings[4 * (j - 1) + 3])
            c[i, j] = parse(Float64, substrings[4 * (j - 1) + 4])
        end
    end

    # close file
    close(file)
    # return received arrays
    return x, y, z, c

end

# function to build plot using obtained data (coordinates and value function)
function buildPlot(x, y, z, c, iter)

    # build plot
    surfPlot = surface(x, y, z, fill_z = c, c = :deep)
    # save plot
    png(surfPlot, filenameImage * string(iter) * ".png")

end

# receive parameters from terminal
filename, iter = receiveParametersFromFilename()
# read data about coordinates and value function from file
x, y, z, c = readDataFromFile(filename)
# build plot using obtained data
buildPlot(x, y, z, c, iter)
